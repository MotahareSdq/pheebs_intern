var express = require('express');
var router = express.Router();
var request = require('request');
var unirest = require("unirest");

router.get('/:city', function(request,response){
  var req = unirest("GET", "http://api.openweathermap.org/data/2.5/weather"); 
    // request('api.openweathermap.org/data/2.5/weather?q=' + req.params.city + '&appid=428a8b832fcbc1afeaeb8f00ddddbb99', function (error, response, body) {
    //   if (!error && response.statusCode == 200) {
    //     res.end(body); 
    //   }
    // })
    req.query({
      "q": request.params.city,
      "appid": '428a8b832fcbc1afeaeb8f00ddddbb99'
    });
    
    req.end(function (res) {
      //var obj = JSON.parse(toString(res.body));
      try{
      temp = (parseFloat(res.body.main.temp)- 273.15).toFixed(2)
      response.send(request.params.city + " : " + temp + " C");
      }
      catch(err){
      response.send('invalid city name');
      }
    });
  } );
  
module.exports = router ;
