var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({ extended: false })

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index.ejs', { title: 'Express' });
});
router.post('/sayHello', urlencodedParser, function (req, res) {
  res.end("Hello " + req.body.name);
});


module.exports = router;
